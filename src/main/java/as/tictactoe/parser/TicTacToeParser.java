package as.tictactoe.parser;

import as.tictactoe.TicTacToe;

/**
 * Created by adam on 6/15/17.
 */
public interface TicTacToeParser {
    TicTacToe parseFrom(String boardString, String player, int turn);
}
