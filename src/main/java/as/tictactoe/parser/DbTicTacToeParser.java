package as.tictactoe.parser;

import as.tictactoe.BoardContext;
import as.tictactoe.Player;
import as.tictactoe.TicTacToe;

import java.util.regex.Pattern;

/**
 * Created by adam on 6/15/17.
 */
public class DbTicTacToeParser implements TicTacToeParser {

    private static final String INPUT_CANNOT_BE_EMPTY = "Parser's Input Parameters Cannot Be Empty";

    @Override
    public TicTacToe parseFrom(String boardString, String player, int turn) {
        throwIfAnyParamEmpty(boardString, player, turn);

        char[][] result = new char[3][3];
        String[] boardSplit = boardString.split(Pattern.quote(String.valueOf(BoardContext.BOARD_CELL_DELIMITER)));
        for (int i = 0; i < result.length; i++) {
            for (int j = 0; j < result[i].length; j++) {
                char dbBoardEntry = boardSplit[i * 3 + j].charAt(0);
                result[i][j] = translateToBoardEntry(dbBoardEntry);
            }
        }
        return new TicTacToe(result, Player.valueOf(player), turn);
    }

    private void throwIfAnyParamEmpty(String boardString, String player, int turn) {
        if(boardString.isEmpty() || player.isEmpty()){
            throw new IllegalArgumentException(INPUT_CANNOT_BE_EMPTY);
        }
    }

    private char translateToBoardEntry(char dbBoardEntry) {
        return dbBoardEntry == BoardContext.EMPTY_FIELD ? BoardContext.EMPTY_CHARACTER : dbBoardEntry;
    }
}
