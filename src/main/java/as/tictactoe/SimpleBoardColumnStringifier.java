package as.tictactoe;

import java.util.StringJoiner;

/**
 * Created by adam on 5/1/17.
 */
public class SimpleBoardColumnStringifier implements BoardColumnStringifier {
    private final String cellDelimiter;
    private final char emptyField;

    public SimpleBoardColumnStringifier(String cellDelimiter, char emptyField) {
        this.cellDelimiter = cellDelimiter;
        this.emptyField = emptyField;
    }

    @Override
    public CharSequence stringifyRow(char[] boardColumn) {
        StringJoiner joiner = new StringJoiner(cellDelimiter);
        for(char c : boardColumn){
            if(c == '\u0000'){
                c = emptyField;
            }
            joiner.add(String.valueOf(c));
        }

        return joiner.toString();
    }
}
