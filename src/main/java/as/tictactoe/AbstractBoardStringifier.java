package as.tictactoe;

import java.util.Arrays;
import java.util.StringJoiner;

/**
 * Created by adam on 5/1/17.
 */
public abstract class AbstractBoardStringifier implements BoardStringifier {
    private final BoardColumnStringifier rowStringifier;
    private final String rowJoinerStr;

    AbstractBoardStringifier(BoardColumnStringifier rowStringifier, String rowJoinerStr) {
        this.rowStringifier = rowStringifier;
        this.rowJoinerStr = rowJoinerStr;
    }

    @Override
    public String convert(char[][] board) {
        StringJoiner rowJoiner = new StringJoiner(rowJoinerStr);
        Arrays.stream(board).forEach(boardX -> rowJoiner.add(rowStringifier.stringifyRow(boardX)));
        return rowJoiner.toString();
    }
}
