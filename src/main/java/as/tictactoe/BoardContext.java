package as.tictactoe;

/**
 * Created by adam on 5/23/17.
 */
public class BoardContext {
    public static final int BOARD_SIZE = 3;
    public static final char EMPTY_CHARACTER = '\u0000';
    public static final char BOARD_CELL_DELIMITER = '|';
    public static final char EMPTY_FIELD = '?';
    public static final String ROW_JOINER = "#";
    public static final String SIMPE_STRINGIFIER_CELL_DELIMITER = "  |  ";
    public static final char STRINGIFIER_EMPTY_CELL = ' ';
    public static final String SIMPLE_STRINGIFIER_NEW_ROW = "\n-------------\n";
}
