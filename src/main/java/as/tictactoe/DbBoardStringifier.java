package as.tictactoe;

/**
 * Created by adam on 5/1/17.
 */
public class DbBoardStringifier extends AbstractBoardStringifier {
    DbBoardStringifier() {
        super(new SimpleBoardColumnStringifier(String.valueOf(BoardContext.BOARD_CELL_DELIMITER),
                BoardContext.EMPTY_FIELD),
                BoardContext.ROW_JOINER);
    }
}
