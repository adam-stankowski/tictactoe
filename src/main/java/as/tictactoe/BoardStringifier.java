package as.tictactoe;

/**
 * Created by adam on 4/23/17.
 */
public interface BoardStringifier {
    String convert(char[][] board);
}
