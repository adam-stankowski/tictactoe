package as.tictactoe;

/**
 * Created by adam on 4/23/17.
 */
public class SimpleBoardStringifier extends AbstractBoardStringifier {

    public SimpleBoardStringifier() {
        super(new SimpleBoardColumnStringifier(BoardContext.SIMPE_STRINGIFIER_CELL_DELIMITER, BoardContext.STRINGIFIER_EMPTY_CELL), BoardContext.SIMPLE_STRINGIFIER_NEW_ROW);
    }
}
