package as.tictactoe;

/**
 * Created by adam on 5/1/17.
 */
public interface BoardColumnStringifier {
    CharSequence stringifyRow(char[] boardColumn);
}
