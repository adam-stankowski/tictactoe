package as.tictactoe;

import java.sql.SQLException;

/**
 * Created by adam on 5/7/17.
 */
public interface TicTacToeReader {
    TicTacToe read() throws SQLException;
}
