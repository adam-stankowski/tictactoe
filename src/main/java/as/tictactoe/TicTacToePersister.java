package as.tictactoe;

import java.sql.SQLException;

/**
 * Created by adam on 5/1/17.
 */
public interface TicTacToePersister {
    String getDatabaseName();

    void persistGameSnapshot(char[][] board, Player player, int turn) throws SQLException;
}
