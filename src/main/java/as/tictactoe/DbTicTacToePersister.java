package as.tictactoe;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created by adam on 4/29/17.
 */
public class DbTicTacToePersister implements TicTacToePersister {
    static final String INSERT_MOVE = "INSERT INTO Game(player,turn,board) VALUES (?,?,?);";
    static final String UPDATE_GAME = "UPDATE Game SET GAME_STR=? WHERE id=?";
    private DbManager dbManager;
    private BoardStringifier boardStringifier;


    public DbTicTacToePersister(DbManager dbManager) {
        this.dbManager = dbManager;
        this.boardStringifier = new DbBoardStringifier();
    }

    @Override
    public String getDatabaseName() {
        return dbManager.getDatabaseName();
    }

    @Override
    public void persistGameSnapshot(char[][] board, Player player, int turn) throws SQLException {
        Connection connection = dbManager.getConnection();
        String statementQuery = turn > 1 ? UPDATE_GAME : INSERT_MOVE;
        PreparedStatement statement = dbManager.getPreparedStatement(connection, statementQuery);
        statement.setString(1, player.toString());
        statement.setInt(2, turn);
        statement.setString(3, boardStringifier.convert(board));
        statement.execute();
        statement.close();
        connection.close();
    }
}
