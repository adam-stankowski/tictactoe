package as.tictactoe;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created by adam on 5/1/17.
 */
public interface DbManager {
    Connection getConnection() throws SQLException;
    PreparedStatement getPreparedStatement(Connection con, String sql) throws SQLException;
    String getDatabaseName();
}
