package as.tictactoe;

import as.tictactoe.parser.TicTacToeParser;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.regex.Pattern;

/**
 * Created by adam on 5/7/17.
 */
public class DbTicTacToeReader implements TicTacToeReader {

    private static final String SELECT_UNFINISHED_EXISTS = "SELECT COUNT(*) FROM Game WHERE finished=FALSE";
    private static final String SELECT_LAST_UNFINISHED = "SELECT board, player, turn FROM Game WHERE finished=FALSE";
    private static final String CANNOT_RESTORE_FROM_DB = "Could not restore saved game parseFrom DB";
    private final DbManager dbManager;
    private final TicTacToeParser ticTacToeParser;

    public DbTicTacToeReader(DbManager dbManager, TicTacToeParser ticTacToeParser) {
        this.dbManager = dbManager;
        this.ticTacToeParser = ticTacToeParser;
    }

    @Override
    public TicTacToe read() throws SQLException {

        if (unfinishedGameExists(dbManager)) {
            return getLastUnfinishedGame();
        }
        //TODO cannot configure TicTacToe to persist moves if initialized this way
        return new TicTacToe();
    }

    private boolean unfinishedGameExists(DbManager dbManager) throws SQLException {
        Connection connection = dbManager.getConnection();
        PreparedStatement preparedStatement = dbManager.getPreparedStatement(connection, SELECT_UNFINISHED_EXISTS);
        ResultSet resultSet = preparedStatement.executeQuery();
        boolean recordsExist = unfinishedGameRecordExists(resultSet);
        resultSet.close();
        preparedStatement.close();
        connection.close();
        return recordsExist;
    }

    private boolean unfinishedGameRecordExists(ResultSet resultSet) throws SQLException {
        if (resultSet.next()) {
            return resultSet.getInt(1) > 0;
        }

        return false;
    }

    private TicTacToe getLastUnfinishedGame() throws SQLException {
        //TODO Refactor code duplication
        Connection connection = dbManager.getConnection();
        PreparedStatement preparedStatement = dbManager.getPreparedStatement(connection, SELECT_LAST_UNFINISHED);
        ResultSet resultSet = preparedStatement.executeQuery();
        TicTacToe savedGame = getGameFromRecord(resultSet);
        resultSet.close();
        preparedStatement.close();
        connection.close();
        return savedGame;
    }

    private TicTacToe getGameFromRecord(ResultSet resultSet) throws SQLException {
        final String boardString, player;
        final int turn;

        if (!resultSet.next()) {
            throw new RuntimeException(CANNOT_RESTORE_FROM_DB);
        }

        boardString = resultSet.getString(1);
        player = resultSet.getString(2);
        turn = resultSet.getInt(3);
        return ticTacToeParser.parseFrom(boardString, player, turn);
    }
}
