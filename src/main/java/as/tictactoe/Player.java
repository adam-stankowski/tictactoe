package as.tictactoe;

/**
 * Created by adam on 4/22/17.
 */
public enum Player {
    X('X'),
    O('O');

    private char sign;

    Player(char sign){
        this.sign = sign;
    }

    public char getSign() {
        return sign;
    }
}
