package as.tictactoe;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created by adam on 5/1/17.
 */
public class DbManagerImpl implements DbManager {
    private final String databaseName;
    private final String serverName;
    private final String portNumber;
    private final String dbms;
    private final String databaseUsername;
    private final String databasePassword;

    public DbManagerImpl(String databaseName, String serverName, String portNumber, String dbms, String databaseUsername, String databasePassword) {
        this.databaseName = databaseName;
        this.serverName = serverName;
        this.portNumber = portNumber;
        this.dbms = dbms;
        this.databaseUsername = databaseUsername;
        this.databasePassword = databasePassword;
    }

    @Override
    public Connection getConnection() throws SQLException {
        return DriverManager.getConnection(constructConnectionString(), databaseUsername, databasePassword);
    }

    @Override
    public PreparedStatement getPreparedStatement(Connection con, String sql) throws SQLException {
        return con.prepareStatement(sql);
    }

    @Override
    public String getDatabaseName() {
        return databaseName;
    }

    private String constructConnectionString() {
        return String.format("jdbc:%s://%s:%s/%s?useLegacyDatetimeCode=false&serverTimezone=UTC", this.dbms, this.serverName, this.portNumber, this.databaseName);
    }
}
