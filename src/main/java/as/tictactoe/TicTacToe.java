package as.tictactoe;

import com.google.common.collect.Lists;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Optional;
import java.util.StringJoiner;
import java.util.UUID;
import java.util.regex.Pattern;

/**
 * Created by adam on 4/20/17.
 */
public class TicTacToe {
    private static final String OUTSIDE_OF_BOUNDS = "Item cannot be placed outside of board";
    private static final String OCCUPIED = "Item cannot ba placed on occupied space";
    private final char[][] board;
    private final boolean persistMoves;
    private Player nextPlayer;
    private Player winner;
    private TicTacToePersister persister;
    private int turn;


    public TicTacToe(boolean persistMoves) {
        this.persistMoves = persistMoves;
        board = new char[BoardContext.BOARD_SIZE][BoardContext.BOARD_SIZE];
        nextPlayer = Player.X;
        winner = null;
        turn = 1;
    }

    public TicTacToe(TicTacToePersister persister) {
        this(true);
        this.persister = persister;
    }

    public TicTacToe() {
        this(false);
    }

    public TicTacToe(char[][] boardArray, Player player, int turn) {
        //TODO maybe I want to persist moves here ?
        this.persistMoves = false;
        this.board = boardArray;
        nextPlayer = player;
        this.turn = turn;
    }

    public void put(int x, int y) throws SQLException {
        checkInsideBoard(x);
        checkInsideBoard(y);

        if (board[x][y] != BoardContext.EMPTY_CHARACTER) {
            throw new RuntimeException(OCCUPIED);
        }

        board[x][y] = nextPlayer.getSign();
        winner = checkWinner();

        if (isPersistMoves()) {
            persister.persistGameSnapshot(board, nextPlayer, turn++);
        }

        nextPlayer = switchPlayer();
    }

    private void checkInsideBoard(int dimension) {
        if (dimension < 0 || dimension > board.length - 1) {
            throw new RuntimeException(String.format(OUTSIDE_OF_BOUNDS));
        }
    }

    protected Player getNextPlayer() {
        return nextPlayer;
    }

    private Player switchPlayer() {
        if (nextPlayer == Player.X) {
            return Player.O;
        }
        return Player.X;
    }

    public Optional<Player> getWinner() {
        return Optional.ofNullable(winner);
    }

    private Player checkWinner() {
        String sequence = boardIntoString(board);
        char sign = nextPlayer.getSign();
        Pattern down = Pattern.compile(String.format("%c{3}", sign));
        Pattern horizontal1 = Pattern.compile(String.format("%c.{2},%c.{2},%c", sign, sign, sign));
        Pattern horizontal2 = Pattern.compile(String.format(".%c.,.%c.,.%c", sign, sign, sign));
        Pattern horizontal3 = Pattern.compile(String.format("(.{2}%c,.{2}%c,.{2}%c)", sign, sign, sign));
        Pattern diagonalLeft = Pattern.compile(String.format("%c.{2},.%c.,.{2}%c", sign, sign, sign));
        Pattern diagonalRight = Pattern.compile(String.format(".{2}%c,.%c.,%c.{2}", sign, sign, sign));
        ArrayList<Pattern> patterns = Lists.newArrayList(down, horizontal1, horizontal2, horizontal3, diagonalLeft, diagonalRight);

        boolean hasWinner = patterns.stream().anyMatch(pattern -> pattern.matcher(sequence).find());

        if (hasWinner) {
            return nextPlayer;
        }
        return null;
    }

    private String boardIntoString(char[][] board) {
        StringJoiner joiner = new StringJoiner(",");
        for (char[] subBoard : board) {
            joiner.add(String.valueOf(subBoard));
        }
        return joiner.toString();
    }

    public boolean isPersistMoves() {
        return persistMoves;
    }

    public char[][] getBoard() {
        return board;
    }
}
