package as.tictactoe;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mockito;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;


/**
 * Created by adam on 4/29/17.
 */
public class DbTicTacToePersisterTest {
    @Rule
    public ExpectedException expectedException = ExpectedException.none();
    private TicTacToePersister persister;
    private DbManager dbManager;
    private Connection connection;
    private PreparedStatement preparedStatement;
    private char[][] board;

    @Before
    public void setUp() throws Exception {
        dbManager = Mockito.mock(DbManager.class);
        persister = Mockito.spy(new DbTicTacToePersister(dbManager));
        connection = Mockito.mock(Connection.class);
        preparedStatement = Mockito.mock(PreparedStatement.class);
        Mockito.doReturn(connection).when(dbManager).getConnection();
        Mockito.doReturn(preparedStatement).when(dbManager).getPreparedStatement(any(Connection.class), anyString());
        board = new char[3][3];
        board[0][0] = Player.X.getSign();
        board[0][1] = Player.O.getSign();
        board[1][2] = Player.O.getSign();
        board[2][1] = Player.X.getSign();
    }

    @Test
    public void whenInstantiatedThenDbHasDbNameTicTacToe() {
        Mockito.doReturn("tictactoe").when(dbManager).getDatabaseName();
        assertThat(persister.getDatabaseName(), is(equalTo("tictactoe")));
    }

    @Test
    public void whenSaveMoveThenConnectionCommit() throws SQLException {
        persister.persistGameSnapshot(board, Player.X, 2);
        Mockito.verify(preparedStatement).setString(1, Player.X.toString());
        Mockito.verify(preparedStatement).setInt(2, 2);
        Mockito.verify(preparedStatement).execute();
        Mockito.verify(connection, Mockito.times(1)).close();
    }

    @Test
    public void whenExceptionInSaveMoveThenThrow() throws SQLException {
        Mockito.doThrow(new SQLException("Exception")).when(dbManager).getConnection();
        expectedException.expect(SQLException.class);
        persister.persistGameSnapshot(board, Player.O, 1);
    }

    @Test
    public void whenSavingSubsequentTurnThenUpdate() throws SQLException {
        persister.persistGameSnapshot(board, Player.O, 2);
        Mockito.verify(dbManager).getPreparedStatement(connection, DbTicTacToePersister.UPDATE_GAME);
        Mockito.verify(connection, Mockito.times(1)).close();
    }

    @Test
    public void whenSavingTurnThenBoardIsSavedAsString() throws SQLException {
        String expectedBoardString = "X|O|?#?|?|O#?|X|?";
        persister.persistGameSnapshot(board, Player.X, 1);
        Mockito.verify(preparedStatement).setString(1, Player.X.toString());
        Mockito.verify(preparedStatement).setString(3, expectedBoardString);
    }
}