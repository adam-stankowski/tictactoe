package as.tictactoe;

import com.github.npathai.hamcrestopt.OptionalMatchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.*;

import java.sql.SQLException;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;


/**
 * Created by adam on 4/20/17.
 */
public class TicTacToeTest {
    TicTacToe ticTacToe;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void setUp() throws Exception {
        ticTacToe = new TicTacToe(false);
    }

    @Test
    public void whenPlaceItemOutsideOfXBoundsThenException() throws SQLException {
        expectedException.expect(RuntimeException.class);
        expectedException.expectMessage("cannot be placed outside of board");
        ticTacToe.put(4, 0);
    }

    @Test
    public void whenPlaceItemOutsideOfYBoundsThenException() throws SQLException {
        expectedException.expect(RuntimeException.class);
        expectedException.expectMessage("cannot be placed outside of board");
        ticTacToe.put(1, 3);
    }

    @Test
    public void whenPlaceItemOutsideOfXYBoundsThenException() throws SQLException {
        expectedException.expect(RuntimeException.class);
        expectedException.expectMessage("cannot be placed outside of board");
        ticTacToe.put(-3, 3);
    }

    @Test
    public void whenPlaceItemOnOccupiedSpaceThenException() throws SQLException {
        ticTacToe.put(1, 1);
        ticTacToe.put(1, 2);
        expectedException.expect(RuntimeException.class);
        ticTacToe.put(1, 1);
    }

    @Test
    public void whenStartingGameThenFirstPlayerIsX() {
        assertThat(ticTacToe.getNextPlayer(), is(Player.X));
    }

    @Test
    public void whenInGameThenPlayerChangesEveryRound() throws SQLException {
        ticTacToe.put(0, 0);
        assertThat(ticTacToe.getNextPlayer(), is(Player.O));
        ticTacToe.put(0, 1);
        assertThat(ticTacToe.getNextPlayer(), is(Player.X));
        ticTacToe.put(0, 2);
        assertThat(ticTacToe.getNextPlayer(), is(Player.O));
    }

    @Test
    public void whenStartingGameThenNoWinner() {
        assertThat(ticTacToe.getWinner(), is(Optional.empty()));
    }

    @Test
    public void whenNoLineThenNoWinner() throws SQLException {
        ticTacToe.put(0, 0);
        ticTacToe.put(1, 0);
        ticTacToe.put(2, 0);
        ticTacToe.put(0, 1);
        assertThat(ticTacToe.getWinner(), is(Optional.empty()));
    }

    @Test
    public void whenThreeXDownThenXIsWinner() throws SQLException {
        ticTacToe.put(0, 0);//X
        ticTacToe.put(1, 0);//O
        ticTacToe.put(0, 1);//X
        ticTacToe.put(1, 1);//O
        ticTacToe.put(0, 2);//X
        assertThat(ticTacToe.getWinner(), OptionalMatchers.hasValue(Player.X));
    }

    @Test
    public void whenThreeXHorizontalThenXIsWinner() throws SQLException {
        ticTacToe.put(0, 0);//X
        ticTacToe.put(0, 1);//O
        ticTacToe.put(1, 0);//X
        ticTacToe.put(1, 1);//O
        ticTacToe.put(2, 0);//X
        assertThat(ticTacToe.getWinner(), OptionalMatchers.hasValue(Player.X));
    }

    @Test
    public void whenThreeXDiagonalLeftThenXIsWinner() throws SQLException {
        ticTacToe.put(0, 0);//X
        ticTacToe.put(0, 1);//O
        ticTacToe.put(1, 1);//X
        ticTacToe.put(1, 0);//O
        ticTacToe.put(2, 2);//X
        assertThat(ticTacToe.getWinner(), OptionalMatchers.hasValue(Player.X));
    }

    @Test
    public void whenThreeXDiagonalRightThenXIsWinner() throws SQLException {
        ticTacToe.put(2, 0);//X
        ticTacToe.put(0, 1);//O
        ticTacToe.put(1, 1);//X
        ticTacToe.put(1, 0);//O
        ticTacToe.put(0, 2);//X
        assertThat(ticTacToe.getWinner(), OptionalMatchers.hasValue(Player.X));
    }

    @Test
    public void whenThreeODownThenOIsWinner() throws SQLException {
        ticTacToe.put(0, 0);//X
        ticTacToe.put(1, 0);//O
        ticTacToe.put(0, 1);//X
        ticTacToe.put(1, 1);//O
        ticTacToe.put(2, 0);//X
        ticTacToe.put(1, 2);//O
        assertThat(ticTacToe.getWinner(), OptionalMatchers.hasValue(Player.O));
    }

    @Test
    public void whenThreeOHorizontalThenOIsWinner() throws SQLException {
        ticTacToe.put(0, 0);//X
        ticTacToe.put(0, 1);//O
        ticTacToe.put(1, 0);//X
        ticTacToe.put(1, 1);//O
        ticTacToe.put(1, 2);//X
        ticTacToe.put(2, 1);//O
        assertThat(ticTacToe.getWinner(), OptionalMatchers.hasValue(Player.O));
    }

    @Test
    public void whenThreeODiagonalLeftThenOIsWinner() throws SQLException {
        ticTacToe.put(1, 0);//X
        ticTacToe.put(0, 0);//O
        ticTacToe.put(2, 0);//X
        ticTacToe.put(1, 1);//O
        ticTacToe.put(2, 1);//X
        ticTacToe.put(2, 2);//O
        assertThat(ticTacToe.getWinner(), OptionalMatchers.hasValue(Player.O));
    }

    @Test
    public void havingPersisterPresentWhenPutThenPersisterSaveMoveExecuted() throws SQLException {
        TicTacToePersister persister = Mockito.mock(TicTacToePersister.class);
        ticTacToe = new TicTacToe(persister);

        ticTacToe.put(1, 0);//X
        Mockito.verify(persister).persistGameSnapshot(ArgumentMatchers.any(char[][].class), ArgumentMatchers.any(Player.class), ArgumentMatchers.anyInt());
    }

    @Test
    public void whenSaveMoveExecutedThenTurnNumberIncreased() throws SQLException {
        TicTacToePersister persister = Mockito.mock(TicTacToePersister.class);
        ticTacToe = new TicTacToe(persister);

        ticTacToe.put(0, 0);
        char[][] board = new char[3][3];
        board[0][0] = Player.X.getSign();
        Mockito.verify(persister).persistGameSnapshot(Mockito.eq(board), Mockito.eq(Player.X), Mockito.eq(1));
        ticTacToe.put(0, 1);
        board[0][1] = Player.O.getSign();
        Mockito.verify(persister).persistGameSnapshot(Mockito.eq(board), Mockito.eq(Player.O), Mockito.eq(2));
    }

}