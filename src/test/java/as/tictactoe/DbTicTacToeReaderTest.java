package as.tictactoe;

import as.tictactoe.parser.TicTacToeParser;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.XMLFormatter;

/**
 * Created by adam on 5/7/17.
 */
public class DbTicTacToeReaderTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    private DbManager dbManager;
    private Connection connection;
    private PreparedStatement preparedStatement;
    private TicTacToeReader ticTacToeReader;
    private TicTacToeParser ticTacToeParser;
    private char[][] expectedReadBoard = new char[][]{{'X', 'O', 'X'}, {EMPTY_CHARACTER, EMPTY_CHARACTER, 'O'}, {EMPTY_CHARACTER, 'X', EMPTY_CHARACTER}};
    private static final String BOARD_STRING = "X|O|X|?|?|O|?|X|?";
    private static final char EMPTY_CHARACTER = '\u0000';

    @Before
    public void setUp() throws Exception {
        dbManager = Mockito.mock(DbManager.class);
        ticTacToeParser = Mockito.mock(TicTacToeParser.class);
        ticTacToeReader = Mockito.spy(new DbTicTacToeReader(dbManager, ticTacToeParser));
        connection = Mockito.mock(Connection.class);
        preparedStatement = Mockito.mock(PreparedStatement.class);
        Mockito.doReturn(connection).when(dbManager).getConnection();
        Mockito.doReturn(preparedStatement).when(dbManager).getPreparedStatement(ArgumentMatchers.any(Connection.class), ArgumentMatchers.anyString());
    }

    @Test
    public void whenNoRecordsInDatabaseThenReturnEmptyTicTacToe() throws SQLException {
        ResultSet noRecordsResultSet = Mockito.mock(ResultSet.class);
        Mockito.doReturn(0).when(noRecordsResultSet).getInt(1);
        Mockito.doReturn(true).when(noRecordsResultSet).next();
        Mockito.doReturn(noRecordsResultSet).when(preparedStatement).executeQuery();
        TicTacToe readGame = ticTacToeReader.read();
        Assert.assertFalse(readGame.getWinner().isPresent());
    }

    @Test
    public void whenExceptionOnConnectingToDbThenThrow() throws SQLException {
        Mockito.doThrow(new SQLException()).when(dbManager).getConnection();
        expectedException.expect(SQLException.class);
        ticTacToeReader.read();
    }

    @Test
    public void whenLastUnfinishedGameExistsThenItsRetrieved() throws SQLException {
        ResultSet oneSavedGameResult = Mockito.mock(ResultSet.class);
        Mockito.doReturn(true).when(oneSavedGameResult).next();
        Mockito.doReturn(1).when(oneSavedGameResult).getInt(1);

        ResultSet savedGameResult = Mockito.mock(ResultSet.class);
        Mockito.doReturn(true).when(savedGameResult).next();
        Mockito.doReturn(BOARD_STRING).when(savedGameResult).getString(1);
        Mockito.doReturn("O").when(savedGameResult).getString(2);
        Mockito.doReturn(6).when(savedGameResult).getInt(3);
        Mockito.doReturn(new TicTacToe(expectedReadBoard, Player.O, 6)).when(ticTacToeParser).parseFrom(ArgumentMatchers.anyString(), ArgumentMatchers.anyString(), ArgumentMatchers.anyInt());

        Mockito.doReturn(oneSavedGameResult, savedGameResult).when(preparedStatement).executeQuery();
        TicTacToe readGame = ticTacToeReader.read();
        Assert.assertFalse(readGame.getWinner().isPresent());
        Assert.assertArrayEquals(expectedReadBoard, readGame.getBoard());
        Mockito.verify(oneSavedGameResult).close();
        Mockito.verify(connection, Mockito.times(2)).close();
    }
}