package as.tictactoe.parser;

import as.tictactoe.BoardContext;
import as.tictactoe.TicTacToe;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

/**
 * Created by adam on 6/15/17.
 */
public class DbTicTacToeParserTest {

    private static final String PLAYED_BOARD = "X|O|?|?|?|O|?|X|?";
    private static final String EMPTY_BOARD = "?|?|?|?|?|?|?|?|?";
    TicTacToeParser parser;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void setUp() throws Exception {
        parser = new DbTicTacToeParser();
    }

    @Test
    public void whenAnyParamEmptyThenException() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Parser's Input Parameters Cannot Be Empty");
        parser.parseFrom("", "", 0);
    }

    @Test
    public void whenParamsNonEmptyThenNoException() {
        parser.parseFrom(PLAYED_BOARD, "X", 1);
    }

    @Test
    public void whenNoMovesOnBoardThenNewTicTacToe() {
        TicTacToe expected = new TicTacToe();
        TicTacToe actual = parser.parseFrom(EMPTY_BOARD, "X", 0);

        Assert.assertArrayEquals(expected.getBoard(), actual.getBoard());
    }

    @Test
    public void whenMovesExistThenRightTicTacToeParsed() {
        char empty = BoardContext.EMPTY_CHARACTER;

        char[][] expectedBoard = new char[][]{{'X', 'O', empty}, {empty, empty, 'O'}, {empty, 'X', empty}};
        TicTacToe actual = parser.parseFrom(PLAYED_BOARD, "X", 0);

        Assert.assertArrayEquals(expectedBoard, actual.getBoard());
    }
}