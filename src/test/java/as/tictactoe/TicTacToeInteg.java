package as.tictactoe;

import org.junit.Test;

import java.sql.SQLException;

/**
 * Created by adam on 5/4/17.
 */
public class TicTacToeInteg {

    private static final String DATABASE_NAME = "tictactoe";
    private static final String SERVER_NAME = "localhost";
    private static final String PORT_NUMBER = "3306";
    private static final String DBMS = "mysql";
    private static final String DATABASE_USER = "tictactoe_user";
    private static final String DATABASE_PASSWORD = "tictactoe_password";

    @Test
    public void whenInitializedThenConnectionMade() throws SQLException {
        DbManager dbManager = new DbManagerImpl(DATABASE_NAME, SERVER_NAME, PORT_NUMBER, DBMS, DATABASE_USER, DATABASE_PASSWORD);
        TicTacToePersister persister = new DbTicTacToePersister(dbManager);
        TicTacToe ticTacToe = new TicTacToe(persister);
        ticTacToe.put(0, 1);
    }
}
