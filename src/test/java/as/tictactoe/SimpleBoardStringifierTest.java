package as.tictactoe;

import org.junit.Before;
import org.junit.Test;

/**
 * Created by adam on 4/23/17.
 */
public class SimpleBoardStringifierTest {
    BoardStringifier boardStringifier;

    @Before
    public void setUp() throws Exception {
        boardStringifier = new SimpleBoardStringifier();
    }

    @Test
    public void whenEmptyBoardThenNoItemsPrinted() {
        char[][] board = new char[3][3];
        System.out.print(boardStringifier.convert(board));
    }

    @Test
    public void whenItemsOnBoardThenPrinted() {
        char[][] board = new char[3][3];
        board[0][0] = Player.X.getSign();
        board[0][1] = Player.X.getSign();
        board[2][0] = Player.X.getSign();
        board[2][1] = Player.O.getSign();
        board[0][2] = Player.O.getSign();
        board[1][1] = Player.O.getSign();
        System.out.print(boardStringifier.convert(board));
    }

}